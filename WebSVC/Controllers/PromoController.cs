﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using CintaIDAPI;


namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class PromoController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public PromoController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        [HttpGet("{Username}/{Password}/{dt}/{keys}/{prm_nama}/{Merchant_ID}")]
        public async Task Get(string Username, string Password, string dt, string keys, string prm_Nama,double Merchant_ID)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("Exec SP_Get_Promo @prm_nama,@Merchant_ID ");
                    cmd.Parameters.AddWithValue("prm_nama", prm_Nama);
                    cmd.Parameters.AddWithValue("Merchant_ID", Merchant_ID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(Username);

            }
        }
        

        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Customer_ID}/{Merchant_ID}/{Event_ID}/{Item_ID}/{Search}")]
        public async Task Get(string Username, string Password, string dt, string keys,double Customer_ID, double Merchant_ID, double Event_ID, double Item_ID, double Search)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("Exec SP_Get_Promo @Customer_ID,@Merchant_ID,@Event_ID,@Item_ID,@Search ");
                cmd.Parameters.AddWithValue("Customer_ID", Customer_ID);
                cmd.Parameters.AddWithValue("Merchant_ID", Merchant_ID);
                cmd.Parameters.AddWithValue("Event_ID", Event_ID);
                cmd.Parameters.AddWithValue("Item_ID", Item_ID);
                cmd.Parameters.AddWithValue("Search", Search);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
               await INS_Intruders(Username);

            }          
          }

        //// GET api/Todo/5
        //[HttpGet("{Username}/{Password}/{dt}/{keys}/{id}")]
        //public async Task Get(string Username, string Password, string dt, string keys, double id)
        //{
        //    if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
        //    {
        //        var cmd = new SqlCommand("Exec SP_Get_Galleries_id @id ");
        //        cmd.Parameters.AddWithValue("id", id);
        //        await SqlPipe.Stream(cmd, Response.Body, "{}");
        //    }
        //    else
        //    {
        //        await INS_Intruders(Username);

        //    }
        //}

        public async Task INS_Intruders(string tr_LogsJson)
            {
                string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
                cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }



        // POST api/Todo
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public async Task Post(string Username, string Password, string dt, string keys)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                string Tr_promoJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand(@"EXEC Ins_Tr_Promo_Json @Tr_promoJson");
                cmd.Parameters.AddWithValue("Tr_promoJson", Tr_promoJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");

            }
            else
            {
                await INS_Intruders(Username);

            }
        }

        // DELETE api/Todo/5
        [HttpDelete("{Username}/{Password}/{dt}/{keys}")]
        public async Task Delete(string Username, string Password, string dt, string keys)
        {

            string Tr_PromoJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand(@"EXEC Del_Tr_Promo @Tr_PromoJson");
            cmd.Parameters.AddWithValue("@Tr_PromoJson", Tr_PromoJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
            
        }


    }
}

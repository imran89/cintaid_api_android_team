﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using CintaIDAPI;


namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class TypeController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public TypeController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Division}/{Module}/{Search}")]
        public async Task Get(string Username, string Password, string dt,string keys,string Division,string Module,string Search)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
            var cmd = new SqlCommand("Exec SP_Get_Type @Division,@Module,@search");
            cmd.Parameters.AddWithValue("@Division", Division);
            cmd.Parameters.AddWithValue("@Module", Module);
            cmd.Parameters.AddWithValue("@Search", Search);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
           }
           else
           {
                await INS_Intruders(Username);

           }
        }
        //SP_Get_TypeName_Json @Division
        //ID1 = IGNORE, ID2 = IGNORE
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Division}/{ID1}/{ID2}/{Search}")]
        public async Task GetTypeName(string Username, string Password, string dt, string keys, string Division, string ID1, string ID2, string Search)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("Exec SP_Get_TypeName_Json @Division, @Search");
                cmd.Parameters.AddWithValue("@Division", Division);
                cmd.Parameters.AddWithValue("@Search", Search);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(Username);

            }
        }

        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Division}/{MerchantID}")]
        public async Task GetTypeByMerchant(string Username, string Password, string dt, string keys, string Division, int MerchantID)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("Exec SP_Get_TypeItem_Merchant_ID @Division, @MerchantID");
                cmd.Parameters.AddWithValue("@Division", Division);
                cmd.Parameters.AddWithValue("@MerchantID", MerchantID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(Username);

            }
        }

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

    }
}

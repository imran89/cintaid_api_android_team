﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;

namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class MerchantController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public MerchantController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }
              
        
        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}")]
        public async Task Get(string Username, string Password, string dt, string keys)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt ))
            {
                var cmd = new SqlCommand("EXEC [dbo].[Get_M_Merchant_Json]");
                
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);
                 
            }

        }

        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Merchant_ID}")]
        public async Task Get(string Username, string Password, string dt, string keys, double Merchant_ID)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("EXEC [Get_M_Merchant_ID] @Merchant_ID");
                cmd.Parameters.AddWithValue("@Merchant_ID", Merchant_ID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }

        }

        //[HttpPost("{Username}/{dt}/{keys}")]
        //public async Task Post(string Username, string dt, string keys)
        //{
        //    if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
        //    {

        //        string M_MerchantsJson = new StreamReader(Request.Body).ReadToEnd();
        //        var cmd = new SqlCommand("Exec Ins_M_Merchant_Json @M_MerchantsJson");
        //        cmd.Parameters.AddWithValue("@M_MerchantsJson", M_MerchantsJson);
        //        await SqlPipe.Stream(cmd, Response.Body, "{}");

        //    }
        //    else
        //    {
        //        await INS_Intruders(keys);

        //    }
        //}


        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public async Task Post(string Username, string Password, string dt, string keys)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {

                string M_MerchantsJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand("Exec Ins_M_Merchant_Json @M_MerchantsJson");
                cmd.Parameters.AddWithValue("@M_MerchantsJson", M_MerchantsJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");

            }
            else
            {
                await INS_Intruders(keys);

            }
        }
        
        
        // DELETE api/Todo/5
        [HttpDelete("{Username}/{dt}/{keys}")]
        public async Task Delete(string Username, string dt, string keys)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {

                string Merchant_Email = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand(@"EXEC Del_M_Merchant_Json @Merchant_Email");
                cmd.Parameters.AddWithValue("@Merchant_Email", Merchant_Email);
                await SqlPipe.Stream(cmd, Response.Body, "{}");

            }
            else
            {
                await INS_Intruders(keys);

            }

        
        }
    }
}

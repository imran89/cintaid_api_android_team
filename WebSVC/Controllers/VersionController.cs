﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using CintaIDAPI;
using Newtonsoft.Json.Linq;
using Microsoft.SqlServer.Server;
using System.Data;
using System;

namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class VersionController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public VersionController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        [HttpGet("{Username}/{Password}/{dt}/{keys}/{App}")]
        public async Task Get(string Username, string Password, string dt, string keys, string App)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("Exec SP_GetVersion @App ");
                cmd.Parameters.AddWithValue("App", App);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(Username);

            }
        }

        // POST api/Todo
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public async Task Post(string Username, string Password, string dt, string keys)
        {

            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                string M_VersionJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand("Exec Ins_M_Version_Json @M_VersionJson");
                cmd.Parameters.AddWithValue("M_VersionJson", M_VersionJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }
            
        }

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

        // GET api/Todo
        [HttpGet]
        public string Get()
        {
            //return @"{""Version_Code"":""v.3.0"",""Version_Get"":""Interested and ProfileID""}";
            //return @"{""Version_Code"":""v.3.2"",""Version_Get"":""Follows paramJson""}";
            //return @"{""Version_Code"":""v.3.3"",""Version_Get"":""Setting Account""}";
            //return @"{""Version_Code"":""v.3.5"",""Version_Get"":""News ParamJson""}";
            //return @"{""Version_Code"":""v.3.6"",""Version_Get"":""Customer Location""}";
            //return @"{""Version_Code"":""v.3.7"",""Version_Get"":""Revisi Place Format paramJson""}";
            //return @"{""Version_Code"":""v.3.9"",""Version_Get"":""Item Group""}";
            //return @"{""Version_Code"":""v.4.0"",""Version_Get"":""PrePaid, PostPaid BillFazz""}";
            //return @"{""Version_Code"":""v.4.1"",""Version_Get"":""Payments jsonFormat""}";
            //return @"{""Version_Code"":""v.4.2"",""Version_Get"":""Transaction""}";
            //return @"{""Version_Code"":""v.4.4"",""Version_Get"":""Partner Credential""}";
            //return @"{""Version_Code"":""v.4.5"",""Version_Get"":""Update Sales DeliveryID""}";
            //return @"{""Version_Code"":""v.4.6"",""Version_Get"":""UOM""}";
            //return @"{""Version_Code"":""v.4.7"",""Version_Get"":""Xendit""}";
            //return @"{""Version_Code"":""v.4.8"",""Version_Get"":""Xendit Respons Production""}";  //
            //return @"{""Version_Code"":""v.4.9"",""Version_Get"":""SP_Get_PrePostPaid_Check""}";
            //return @"{""Version_Code"":""v.5.0"",""Version_Get"":""NearMe.""}";
            //return @"{""Version_Code"":""v.5.1"",""Version_Get"":""Customer Address""}";
            //return @"{""Version_Code"":""v.5.2"",""Version_Get"":""Destination History Delete""}";
            //return @"{""Version_Code"":""v.5.3"",""Version_Get"":""Advertisement""}";
            //return @"{""Version_Code"":""v.5.4"",""Version_Get"":""Top Item TerFavorite ""}";
            //return @"{""Version_Code"":""v.5.5"",""Version_Get"":""Help By Category ""}";
            //return @"{""Version_Code"":""v.5.6"",""Version_Get"":""Have not Rating Merchant ""}";
            //return @"{""Version_Code"":""v.5.7"",""Version_Get"":""Topup Saldo ""}";
            //return @"{""Version_Code"":""v.5.8"",""Version_Get"":""PIN ""}";
            //return @"{""Version_Code"":""v.5.9"",""Version_Get"":""Join ""}";
            //return @"{""Version_Code"":""v.6.1"",""Version_Get"":""Mutation Dev ""}";
            //return @"{""Version_Code"":""v.6.2"",""Version_Get"":""Confirm Transaction By Admin""}";
            //return @"{""Version_Code"":""v.6.3"",""Version_Get"":""Saldo Transaction""}";
            //return @"{""Version_Code"":""v.6.4"",""Version_Get"":""Bank Customer, Withdrawal""}";
            return @"{""Version_Code"":""v.6.5"",""Version_Get"":""Merchant Approval By Admin""}";


        }

   
    }
}

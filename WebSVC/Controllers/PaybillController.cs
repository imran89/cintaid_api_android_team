﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using CintaIDAPI;


namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class PaybillController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public PaybillController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{search}/{type}")]
        public async Task Get(string Username, string Password, string dt, string keys, string search, int type)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("Exec SP_Get_Paybill_Json @search, @type");
                cmd.Parameters.AddWithValue("search", search);
                cmd.Parameters.AddWithValue("type", type);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
               await INS_Intruders(Username);
            }          
          }

            public async Task INS_Intruders(string tr_LogsJson)
            {
                string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
                cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }

        

        

    }
}

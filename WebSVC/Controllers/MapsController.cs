﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using CintaIDAPI;


namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class MapsController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public MapsController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Selection}/{Search}/{Long}/{Lad}/{Rad}/{Tipe}/{UserID}")]
        public async Task Get(string Username, string Password, string dt,string keys,string Selection,string Search, double Long, double Lad, int Rad, string Tipe, int UserID)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
            var cmd = new SqlCommand("Exec SP_Get_MapMerchant @Selection,@Search,@Long,@Lad,@Rad,@Tipe,@UserID");
                cmd.Parameters.AddWithValue("@Selection", Selection);
                cmd.Parameters.AddWithValue("@Search", Search);
                cmd.Parameters.AddWithValue("@Long", Long);
                cmd.Parameters.AddWithValue("@Lad", Lad);
                cmd.Parameters.AddWithValue("@Rad", Rad);
                cmd.Parameters.AddWithValue("@Tipe", Tipe);
                cmd.Parameters.AddWithValue("@UserID", UserID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
           }
           else
           {
                await INS_Intruders(Username);

            }
        }

     

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

    }
}

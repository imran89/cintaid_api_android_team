﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using CintaIDAPI;



namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class EmailController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public EmailController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

    

        // GET api/Todo
        [HttpGet]
        public async Task Get()
        {
            var cmd = new SqlCommand("Exec SP_LoginALL");
            await SqlPipe.Stream(cmd, Response.Body, "{}");


        }

        // GET api/Todo/5
        [HttpGet("{username}/{password}/{key}")]
        public async Task Get(string username,string password,string key)
        {
             if (key == CintaIDAPI.CLS_FUNCT.Base64Encode(username.ToString()))
             {
            

            var cmd = new SqlCommand("Exec SP_Login @username,@password");
            cmd.Parameters.AddWithValue("@username", username);
            cmd.Parameters.AddWithValue("@password", password);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
           }
           else
           {
                INS_Intruders(username);

            }
        }
                public async void INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

        // POST api/Todo
        [HttpPost()]
        public async Task Post()
        {

            //if (key == CintaIDAPI.CLS_FUNCT.Base64Encode(key.ToString()))
            //{

                string LoginJson = new StreamReader(Request.Body).ReadToEnd();
               // INS_Intruders(LoginJson);
                var cmd = new SqlCommand("Exec SP_Login @loginJson");
                cmd.Parameters.AddWithValue("@loginJson", LoginJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
               
            //}
            //else
            //{
            // INS_Intruders(key);

            //}


        }


    }
}

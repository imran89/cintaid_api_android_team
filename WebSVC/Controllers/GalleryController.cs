﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using CintaIDAPI;


namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class GalleryController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public GalleryController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        [HttpGet("{Username}/{Password}/{dt}/{keys}")]
        public async Task Get(string Username, string Password, string dt, string keys)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("Exec SP_Get_FriendList @id ");
                cmd.Parameters.AddWithValue("id", "");
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(Username);

            }
        }
        

        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{Customer_ID}/{Merchant_ID}/{Event_ID}/{Item_ID}/{Search}")]
        public async Task Get(string Username, string Password, string dt, string keys,double Customer_ID, double Merchant_ID, double Event_ID, double Item_ID, double Search)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("Exec SP_Get_Galleries_Search @Customer_ID,@Merchant_ID,@Event_ID,@Item_ID,@Search ");
                cmd.Parameters.AddWithValue("Customer_ID", Customer_ID);
                cmd.Parameters.AddWithValue("Merchant_ID", Merchant_ID);
                cmd.Parameters.AddWithValue("Event_ID", Event_ID);
                cmd.Parameters.AddWithValue("Item_ID", Item_ID);
                cmd.Parameters.AddWithValue("Search", Search);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
               await INS_Intruders(Username);

            }          
          }

        // GET api/Todo/5
        [HttpGet("{Username}/{Password}/{dt}/{keys}/{id}")]
        public async Task Get(string Username, string Password, string dt, string keys, double id)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("Exec SP_Get_Galleries_id @id ");
                cmd.Parameters.AddWithValue("id", id);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(Username);

            }
        }

        public async Task INS_Intruders(string tr_LogsJson)
            {
                string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
                cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }

        

    // POST api/Todo
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public async Task Post(string Username, string Password, string dt, string keys)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
            string Tr_GalleryJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand(@"EXEC Ins_Tr_Galleries_Json @Tr_GalleryJson");
            cmd.Parameters.AddWithValue("Tr_GalleryJson", Tr_GalleryJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");

            }
            else
            {
                await INS_Intruders(Username);

            }
        }

        // DELETE api/Todo/5
        [HttpDelete("{Username}/{Password}/{dt}/{keys}/{id}")]
        public async Task Delete(string Username, string Password, string dt, string keys, double id)
        {
            var cmd = new SqlCommand(@"EXEC Del_TR_Galleries @id");
            cmd.Parameters.AddWithValue("@id", id);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }


    }
}

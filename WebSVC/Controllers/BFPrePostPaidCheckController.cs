﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using CintaIDAPI;


namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class BFPrePostPaidCheckController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public BFPrePostPaidCheckController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        [HttpGet("{Username}/{Password}/{dt}/{keys}/{paramJson}")]
        public async Task Get(string Username, string Password, string dt, string keys, string paramJson)
        {
            string deCode = "";
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                deCode = CintaIDAPI.CLS_FUNCT.Base64Decode(paramJson);

                var cmd = new SqlCommand("Exec SP_Get_PrePostPaid_Check @paramJson ");
                cmd.Parameters.AddWithValue("paramJson", deCode);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(Username);

            }
        }
        

        

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }


        //// POST api/Todo
        //[HttpPost("{Username}/{Password}/{dt}/{keys}")]
        //public async Task Post(string Username, string Password, string dt, string keys)
        //{
        //    //if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
        //    //{
        //    string Tr_InterestedJson = new StreamReader(Request.Body).ReadToEnd();
        //    var cmd = new SqlCommand(@"EXEC Ins_Tr_BillFazz_Order_PrePaid_Json @Tr_InterestedJson");
        //    cmd.Parameters.AddWithValue("Tr_InterestedJson", Tr_InterestedJson);
        //    await SqlPipe.Stream(cmd, Response.Body, "{}");

        //    //}
        //    //else
        //    //{
        //    //    await INS_Intruders(Username);

        //    //}
        //}

        //// DELETE api/Todo/5
        //[HttpDelete("{Username}/{Password}/{dt}/{keys}")]
        //public async Task Delete(string Username, string Password, string dt, string keys)
        //{
        //    string Tr_InterestedJson = new StreamReader(Request.Body).ReadToEnd();
        //    var cmd = new SqlCommand(@"EXEC Del_BF_PrePaid @BF_PrePaidJson");
        //    cmd.Parameters.AddWithValue("@BF_PrePaidJson", Tr_InterestedJson);
        //    await SqlPipe.Stream(cmd, Response.Body, "{}");
        //}


    }
}

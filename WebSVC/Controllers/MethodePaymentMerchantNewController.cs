﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using CintaIDAPI;


namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class MethodePaymentMerchantNewController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public MethodePaymentMerchantNewController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        [HttpGet("{Username}/{Password}/{dt}/{keys}/{MerchantID}")]
        public async Task Get(string Username, string Password, string dt, string keys,double MerchantID)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("Exec SP_Get_Tr_MethodePaymentMerchant_NEW @MerchantID ");
                cmd.Parameters.AddWithValue("MerchantID", MerchantID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(Username);

            }
        }
        
        

        public async Task INS_Intruders(string tr_LogsJson)
        {
            string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
            cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }

        

    // POST api/Todo
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public async Task Post(string Username, string Password, string dt, string keys)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
            string Tr_MethodePaymentMerchantJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand(@"EXEC Ins_Tr_MethodePaymentMerchantNew_Json @Tr_MethodePaymentMerchantNewJson");
            cmd.Parameters.AddWithValue("Tr_MethodePaymentMerchantJson", Tr_MethodePaymentMerchantJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");

            }
            else
            {
                await INS_Intruders(Username);

            }
        }
        


    }
}

﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using CintaIDAPI;


namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public ProductController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        [HttpGet("{Username}/{Password}/{dt}/{keys}/{MerchantApps}/{Merchant_ID}/{search}")]
        public async Task Get(string Username, string Password, string dt, string keys, string MerchantApps, double Merchant_ID, string search)
        {

            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("EXEC SP_get_Products_Search @MerchantApps, @Merchant_ID, @search");
                cmd.Parameters.AddWithValue("MerchantApps", MerchantApps);
                cmd.Parameters.AddWithValue("Merchant_ID", Merchant_ID);
                cmd.Parameters.AddWithValue("search", search);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(keys);

            }

        }


        
            public async Task INS_Intruders(string tr_LogsJson)
            {
                string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
                cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
        
    }
}

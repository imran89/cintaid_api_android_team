﻿using Belgrade.SqlClient;
using Microsoft.AspNetCore.Mvc;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using CintaIDAPI;


namespace CintaIDAPI.Controllers
{
    [Route("api/[controller]")]
    public class StoriesController : Controller
    {
        private readonly IQueryPipe SqlPipe;
        private readonly ICommand SqlCommand;

        public StoriesController(ICommand sqlCommand, IQueryPipe sqlPipe)
        {
            this.SqlCommand = sqlCommand;
            this.SqlPipe = sqlPipe;
        }

        [HttpGet("{Username}/{Password}/{dt}/{keys}/{id}/{Type_ID}/{Long}/{Lat}/{Radius}/{Username_ID}")]
        public async Task Get(string Username, string Password, string dt, string keys, double id, string Type_ID,double Long, double Lat, int Radius, double Username_ID)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
                var cmd = new SqlCommand("Exec [SP_GET_StorieS] @id, @Type_ID, @Long, @Lat, @Radius, @Username_ID ");
                cmd.Parameters.AddWithValue("id", id);
                cmd.Parameters.AddWithValue("Type_ID", Type_ID);
                cmd.Parameters.AddWithValue("Long", Long);
                cmd.Parameters.AddWithValue("Lat", Lat);
                cmd.Parameters.AddWithValue("Radius", Radius);
                cmd.Parameters.AddWithValue("Username_ID", Username_ID);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }
            else
            {
                await INS_Intruders(Username);

            }
        }
        
        

        public async Task INS_Intruders(string tr_LogsJson)
            {
                string Tr_LogsJson = new StreamReader(Request.Body).ReadToEnd();
                var cmd = new SqlCommand("exec SP_INS_LOGS @Tr_LogsJson");
                cmd.Parameters.AddWithValue("@Tr_LogsJson", tr_LogsJson);
                await SqlPipe.Stream(cmd, Response.Body, "{}");
            }

        

    // POST api/Todo
        [HttpPost("{Username}/{Password}/{dt}/{keys}")]
        public async Task Post(string Username, string Password, string dt, string keys)
        {
            if (keys == CintaIDAPI.CLS_FUNCT.Base64Encode(Username + "^Cinta.ID-L3xY*0MT*83@$T!3^" + dt))
            {
            string Tr_StoriesJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand(@"EXEC Ins_Tr_Stories_Json @Tr_StoriesJson");
            cmd.Parameters.AddWithValue("Tr_StoriesJson", Tr_StoriesJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");

            }
            else
            {
                await INS_Intruders(Username);

            }
        }

        // DELETE api/Todo/5
        [HttpDelete("{Username}/{Password}/{dt}/{keys}")]
        public async Task Delete(string Username, string Password, string dt, string keys)
        {
            string Tr_StoriesJson = new StreamReader(Request.Body).ReadToEnd();
            var cmd = new SqlCommand(@"EXEC Del_TR_Stories @Tr_StoriesJson");
            cmd.Parameters.AddWithValue("Tr_StoriesJson", Tr_StoriesJson);
            await SqlPipe.Stream(cmd, Response.Body, "{}");
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MimeKit;
using MailKit.Security;
using MimeKit.Utils;

namespace CintaIDAPI
{
    public class MessageServices
    {
        public static async Task SendEmailAsync(string email, string subject, string messageHTML, string messageText)
        {
            var emailMessage = new MimeMessage();

            //emailMessage.From.Add(new MailboxAddress("Customer Care", "cares@cinta.co.id"));
            emailMessage.From.Add(new MailboxAddress("Customer Care", "cs@cinta.id"));
            emailMessage.To.Add(new MailboxAddress("", email));
            emailMessage.Subject = subject;

            var bodyBuilder = new BodyBuilder();
            bodyBuilder.HtmlBody = messageHTML;
            bodyBuilder.TextBody = messageText;

           
            emailMessage.Body = bodyBuilder.ToMessageBody();

            //emailMessage.Body = new TextPart("plain") { Text = message };

            using (var client = new SmtpClient())
            {
                client.LocalDomain = "cinta.co.id";
                await client.ConnectAsync("cinta.id", 25, SecureSocketOptions.None).ConfigureAwait(false);
                //await client.AuthenticateAsync("Cares@cinta.co.id", "Bigmac.2011"); 
                await client.AuthenticateAsync("cs@cinta.id", "Good.P@$$w0rd");
                await client.SendAsync(emailMessage).ConfigureAwait(false);
                await client.DisconnectAsync(true).ConfigureAwait(false);
            }
        }
        
    }
}
